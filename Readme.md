# News Aggregator

News Aggregator is an app that provides the latest three headlines


# Run and Install
You must use Android Studio to run this app

## Features
* Show top 3 headlines on app start and refresh with swipe down gesture
* Show the details of each headline


## Improvements
* Provide offline availability so that users can still view the articles even with no Internet
* Provide more tests
* Provide back button behaviour on detail screen
* CNN API currently doesn't show full content text
