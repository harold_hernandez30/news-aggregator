package com.cloudemployee.newsaggregator.newsfeed;

import android.accounts.NetworkErrorException;

import com.cloudemployee.newsaggregator.newsfeed.model.Article;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.List;

import static junit.framework.Assert.assertEquals;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

/**
 * Created by MacBook on 21/03/2019.
 */
public class NewsListingPresenterTest {

    NewsListingPresenter mNewsListingPresenter;

    @Mock
    NewsListingContract.View mView;

    @Mock
    CnnNewsRepository mCnnRepository;

    @Captor
    private ArgumentCaptor<Boolean> mShowLoadingIndicatorArgumentCaptor;

    @Captor
    private ArgumentCaptor<List<Article>> mArticlesArgumentCaptor;

    @Captor
    private ArgumentCaptor<CnnNewsDatasource.ArticlesCallback> mArticlesCallbackArgumentCaptor;

    @Captor
    private ArgumentCaptor<String> errorMessageArgumentCaptor;

    private List<Article> mArticles;

    @Before
    public void setUp() throws Exception {

        InputStream in = getClass().getClassLoader().getResourceAsStream("articles.json");

        BufferedReader r = new BufferedReader(new InputStreamReader(in));
        StringBuilder total = new StringBuilder();
        String line;
        while ((line = r.readLine()) != null) {
            total.append(line).append('\n');
        }

        mArticles = new Gson().fromJson(total.toString(), new TypeToken<List<Article>>() {
        }.getType());


        MockitoAnnotations.initMocks(this);
        mNewsListingPresenter = new NewsListingPresenter(mView, mCnnRepository);
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void testShowLoadingScreenOnInit() throws Exception {
        mNewsListingPresenter.init();

        verify(mView).showLoadingIndicator(mShowLoadingIndicatorArgumentCaptor.capture());

        assertEquals(Boolean.TRUE, mShowLoadingIndicatorArgumentCaptor.getValue());
    }

    @Test
    public void testShowThreeTopHeadlines() throws Exception {
        mNewsListingPresenter.init();

        //Test loading indicator to show
        verify(mView).showLoadingIndicator(mShowLoadingIndicatorArgumentCaptor.capture());
        assertEquals(Boolean.TRUE, mShowLoadingIndicatorArgumentCaptor.getValue());

        verify(mCnnRepository).getArticles(mArticlesCallbackArgumentCaptor.capture());
        mArticlesCallbackArgumentCaptor.getValue().onReceived(mArticles);

        //Test loading indicator to not show
        verify(mView, times(2)).showLoadingIndicator(mShowLoadingIndicatorArgumentCaptor.capture());
        assertEquals(Boolean.FALSE, mShowLoadingIndicatorArgumentCaptor.getValue());

        verify(mView).showTopHeadlines(mArticlesArgumentCaptor.capture());
        assertEquals(mArticles, mArticlesArgumentCaptor.getValue());
    }

    @Test
    public void testShowErrorMessage() throws Exception {
        mNewsListingPresenter.init();

        verify(mCnnRepository).getArticles(mArticlesCallbackArgumentCaptor.capture());
        mArticlesCallbackArgumentCaptor.getValue().onError(new NetworkErrorException());

        verify(mView).showErrorMessage(errorMessageArgumentCaptor.capture());

        verify(mView, times(2))
                .showLoadingIndicator(mShowLoadingIndicatorArgumentCaptor.capture());

        assertEquals(Boolean.FALSE, mShowLoadingIndicatorArgumentCaptor.getValue());
    }

    @Test
    public void testShowEmptyMessage() throws Exception {

        mNewsListingPresenter.init();

        verify(mCnnRepository).getArticles(mArticlesCallbackArgumentCaptor.capture());
        mArticlesCallbackArgumentCaptor.getValue().onEmpty();

        verify(mView, times(2))
                .showLoadingIndicator(mShowLoadingIndicatorArgumentCaptor.capture());
        assertEquals(Boolean.FALSE, mShowLoadingIndicatorArgumentCaptor.getValue());

        verify(mView).showEmptyView();
    }

    @Test
    public void testOnRefreshShouldShowNewArticles() throws Exception {
        mNewsListingPresenter.onRefresh();

        verify(mCnnRepository).getArticles(mArticlesCallbackArgumentCaptor.capture());
        mArticlesCallbackArgumentCaptor.getValue().onReceived(mArticles);

        verify(mView).updateArticles(mArticlesArgumentCaptor.capture());
        assertEquals(mArticles, mArticlesArgumentCaptor.getValue());

        verify(mView).showLoadingIndicator(mShowLoadingIndicatorArgumentCaptor.capture());
        assertEquals(Boolean.FALSE, mShowLoadingIndicatorArgumentCaptor.getValue());
    }

    @Test
    public void testOnRefresh_onError_shouldShowErrorMessage() throws Exception {

        mNewsListingPresenter.onRefresh();

        verify(mCnnRepository).getArticles(mArticlesCallbackArgumentCaptor.capture());
        mArticlesCallbackArgumentCaptor.getValue().onError(new NetworkErrorException());

        verify(mView).showErrorMessage(errorMessageArgumentCaptor.capture());
        assertEquals("Unable to update news", errorMessageArgumentCaptor.getValue());

        verify(mView).showLoadingIndicator(mShowLoadingIndicatorArgumentCaptor.capture());
        assertEquals(Boolean.FALSE, mShowLoadingIndicatorArgumentCaptor.getValue());
    }

    @Test
    public void testOnRefresh_onEmpty_shouldNotShowEmptyView() throws Exception {

        mNewsListingPresenter.onRefresh();

        verify(mCnnRepository).getArticles(mArticlesCallbackArgumentCaptor.capture());
        mArticlesCallbackArgumentCaptor.getValue().onEmpty();

        verify(mView).showLoadingIndicator(mShowLoadingIndicatorArgumentCaptor.capture());
        assertEquals(Boolean.FALSE, mShowLoadingIndicatorArgumentCaptor.getValue());

        verify(mView, never()).showEmptyView();
    }
}