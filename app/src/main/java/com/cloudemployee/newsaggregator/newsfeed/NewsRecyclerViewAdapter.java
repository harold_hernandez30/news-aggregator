package com.cloudemployee.newsaggregator.newsfeed;

import android.os.Build;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.cloudemployee.newsaggregator.R;
import com.cloudemployee.newsaggregator.newsfeed.NewsListingFragment.OnListFragmentInteractionListener;
import com.cloudemployee.newsaggregator.newsfeed.model.Article;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class NewsRecyclerViewAdapter extends RecyclerView.Adapter<NewsRecyclerViewAdapter.ViewHolder> {

    private final List<Article> mArticles;
    private final OnListFragmentInteractionListener mListener;

    public NewsRecyclerViewAdapter(OnListFragmentInteractionListener listener) {
        mArticles = new ArrayList<>();
        mListener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_news, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.mItem = mArticles.get(position);
        holder.mTitle.setText(holder.mItem.getTitle());
        holder.mContentPreview.setText(holder.mItem.getDescription());

        ViewCompat.setTransitionName(holder.mIcon, holder.mItem.getUrlToImage());
        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mListener) {
                    // Notify the active callbacks interface (the activity, if the
                    // fragment is attached to one) that an item has been selected.
                    mListener.onListFragmentInteraction(holder.mItem, holder.mIcon);
                }
            }
        });


        Picasso.with(holder.mView.getContext()).load(holder.mItem.getUrlToImage())
                .error(R.mipmap.ic_launcher)
                .centerCrop()
                .fit()
                .into(holder.mIcon);
    }

    @Override
    public int getItemCount() {
        return mArticles.size();
    }

    public void updateData(List<Article> articles) {
        mArticles.clear();
        mArticles.addAll(articles);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView mTitle;
        public final ImageView mIcon;
        public final TextView mContentPreview;
        public Article mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            mTitle = (TextView) view.findViewById(R.id.news_item_title);
            mContentPreview = (TextView) view.findViewById(R.id.news_item_preview);
            mIcon = (ImageView) view.findViewById(R.id.news_item_image);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + mTitle.getText() + "'";
        }
    }
}
