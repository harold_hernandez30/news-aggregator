package com.cloudemployee.newsaggregator.newsfeed;

import java.util.Map;

/**
 * Created by MacBook on 21/03/2019.
 */

public class CnnNewsRepository implements CnnNewsDatasource{

    private final CnnNewsDatasource mLocalCnnNewsDatasource;
    private final CnnNewsDatasource mRemoteCnnNewsDatasource;

    public CnnNewsRepository(CnnNewsDatasource localCnnNewsDatasource,
                             CnnNewsDatasource remoteCnnNewsDatasource) {
        this.mLocalCnnNewsDatasource = localCnnNewsDatasource;
        this.mRemoteCnnNewsDatasource = remoteCnnNewsDatasource;
    }

    @Override
    public void getArticles(Map<String, String> options, ArticlesCallback articlesCallback) {
        //TODO: provide offline availability by using mLocalCnnNewsDatasource
        mRemoteCnnNewsDatasource.getArticles(options, articlesCallback);

    }
}
