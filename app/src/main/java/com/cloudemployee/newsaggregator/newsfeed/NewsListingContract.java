package com.cloudemployee.newsaggregator.newsfeed;

import com.cloudemployee.newsaggregator.newsfeed.model.Article;

import java.util.List;

/**
 * Created by MacBook on 20/03/2019.
 */

public interface NewsListingContract {

    interface View {

        void showTopHeadlines(List<Article> articles);

        void showLoadingIndicator(boolean shouldShow);

        void showErrorMessage(String errorMessage);

        void updateArticles(List<Article> articles);

        void showEmptyView();
    }

    interface Presenter {

        void init();

        void onRefresh();
    }
}
