package com.cloudemployee.newsaggregator.newsfeed;

import android.support.annotation.NonNull;

import com.cloudemployee.newsaggregator.Config;
import com.cloudemployee.newsaggregator.newsfeed.model.Article;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by MacBook on 21/03/2019.
 */

public class NewsListingPresenter implements NewsListingContract.Presenter {


    private NewsListingContract.View mView;
    private CnnNewsRepository mCnnNewsRepository;
    private int mCurrentPageCount = 1;
    private int mTotalResultCount;

    public NewsListingPresenter(NewsListingContract.View view,
                                CnnNewsRepository cnnNewsRepository) {
        mView = view;
        mCnnNewsRepository = cnnNewsRepository;
    }

    @Override
    public void init() {

        mView.showLoadingIndicator(true);

        Map<String, String> options = updateOptions();
        mCnnNewsRepository.getArticles(options, new CnnNewsDatasource.ArticlesCallback() {
            @Override
            public void onReceived(List<Article> articles, int totalResult) {
                mView.showLoadingIndicator(false);
                mView.showTopHeadlines(articles);
                mTotalResultCount = totalResult;
            }

            @Override
            public void onError(Exception e) {
                //TODO: Use resourceManager to get the string from R.string.error_message
                mView.showErrorMessage("Something went wrong");
                mView.showLoadingIndicator(false);
            }

            @Override
            public void onEmpty() {
                mView.showLoadingIndicator(false);
                mView.showEmptyView();
            }
        });

    }

    @Override
    public void onRefresh() {

        Map<String, String> options = updateOptions();
        mCnnNewsRepository.getArticles(options, new CnnNewsDatasource.ArticlesCallback() {
            @Override
            public void onReceived(List<Article> articles, int totalResult) {
                mView.showLoadingIndicator(false);
                mView.updateArticles(articles);
                mTotalResultCount = totalResult;
            }

            @Override
            public void onError(Exception e) {
                //TODO: Use resourceManager to get the string from R.string.error_message
                mView.showErrorMessage("Unable to update news");
                mView.showLoadingIndicator(false);
            }

            @Override
            public void onEmpty() {
                mView.showLoadingIndicator(false);
            }
        });
    }

    @NonNull
    private Map<String, String> updateOptions() {
        Map<String, String> options = new HashMap<>();
        options.put("country", "us");
        options.put("pageSize", String.valueOf(Config.PAGE_SIZE));
        if ((mCurrentPageCount + 1) < (mTotalResultCount / Config.PAGE_SIZE)) {
            options.put("page", String.valueOf(mCurrentPageCount++));
        } else {
            mCurrentPageCount = 1;
            options.put("page", String.valueOf(mCurrentPageCount++));
        }
        return options;
    }
}
