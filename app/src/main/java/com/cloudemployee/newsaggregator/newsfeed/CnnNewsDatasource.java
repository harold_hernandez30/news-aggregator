package com.cloudemployee.newsaggregator.newsfeed;

import com.cloudemployee.newsaggregator.newsfeed.model.Article;

import java.util.List;
import java.util.Map;

/**
 * Created by MacBook on 21/03/2019.
 */

public interface CnnNewsDatasource {

    interface ArticlesCallback {
        void onReceived(List<Article> articles, int totalResults);

        void onError(Exception e);

        void onEmpty();
    }

    void getArticles(Map<String, String> options, ArticlesCallback articlesCallback);
}
