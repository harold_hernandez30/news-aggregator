package com.cloudemployee.newsaggregator.newsfeed.remote;

import com.cloudemployee.newsaggregator.Config;
import com.cloudemployee.newsaggregator.network.ApiService;
import com.cloudemployee.newsaggregator.newsfeed.CnnNewsDatasource;
import com.cloudemployee.newsaggregator.newsfeed.model.CNNTopHeadlinesResponse;

import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by MacBook on 21/03/2019.
 */

public class CnnNewsRemoteDatasource implements CnnNewsDatasource {

    private ApiService mApiService;

    public CnnNewsRemoteDatasource(ApiService apiService) {
        mApiService = apiService;
    }

    @Override
    public void getArticles(Map<String, String> options, final ArticlesCallback articlesCallback) {
        mApiService.requestTopHeadlines(options).enqueue(new Callback<CNNTopHeadlinesResponse>() {
            @Override
            public void onResponse(Call<CNNTopHeadlinesResponse> call,
                                   Response<CNNTopHeadlinesResponse> response) {
                if (response.isSuccessful()) {
                    CNNTopHeadlinesResponse body = response.body();
                    if (body != null) {
                        articlesCallback.onReceived(body.getArticles(), body.getTotalResults());
                    } else {
                        articlesCallback.onError(new Exception("Unknown Error"));
                    }
                } else {
                    articlesCallback.onError(new Exception(response.message()));
                }
            }

            @Override
            public void onFailure(Call<CNNTopHeadlinesResponse> call, Throwable t) {
                articlesCallback.onError(new Exception(t));
            }
        });
    }
}
