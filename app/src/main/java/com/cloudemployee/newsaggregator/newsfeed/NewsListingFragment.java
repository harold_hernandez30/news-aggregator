package com.cloudemployee.newsaggregator.newsfeed;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.cloudemployee.newsaggregator.BaseFragment;
import com.cloudemployee.newsaggregator.R;
import com.cloudemployee.newsaggregator.newsfeed.model.Article;

import java.util.List;

import javax.inject.Inject;

/**
 * A fragment representing a list of Items.
 * <p/>
 * Activities containing this fragment MUST implement the {@link OnListFragmentInteractionListener}
 * interface.
 */
public class NewsListingFragment extends BaseFragment implements NewsListingContract.View {

    @Inject
    CnnNewsRepository mCnnNewsRepository;

    private OnListFragmentInteractionListener mListener;
    private NewsRecyclerViewAdapter mAdapter;
    private NewsListingPresenter mPresenter;
    private SwipeRefreshLayout mSwipeViewContainer;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public NewsListingFragment() {
    }

    // TODO: Customize parameter initialization
    @SuppressWarnings("unused")
    public static NewsListingFragment newInstance() {
        NewsListingFragment fragment = new NewsListingFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getFragmentComponent().inject(this);
        mPresenter = new NewsListingPresenter(this, mCnnNewsRepository);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_news_list, container, false);
        // Set the adapter
        Context context = view.getContext();
        RecyclerView recyclerView = view.findViewById(R.id.list);
        recyclerView.setLayoutManager(new LinearLayoutManager(context));
        recyclerView.setHasFixedSize(true);
        recyclerView.setItemViewCacheSize(20);
        recyclerView.setDrawingCacheEnabled(true);
        recyclerView.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
        mAdapter = new NewsRecyclerViewAdapter(mListener);
        recyclerView.setAdapter(mAdapter);
        mSwipeViewContainer = view.findViewById(R.id.swipeContainer);

        mSwipeViewContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                mPresenter.onRefresh();
            }
        });

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        mPresenter.init();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnListFragmentInteractionListener) {
            mListener = (OnListFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnListFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void showTopHeadlines(List<Article> articles) {
        mAdapter.updateData(articles);
        mAdapter.notifyDataSetChanged();
    }

    @Override
    public void showLoadingIndicator(boolean shouldShow) {
        mSwipeViewContainer.setRefreshing(shouldShow);
    }

    @Override
    public void showErrorMessage(String errorMessage) {

    }

    @Override
    public void updateArticles(List<Article> articles) {
        mAdapter.updateData(articles);
        mAdapter.notifyDataSetChanged();
    }

    @Override
    public void showEmptyView() {

    }

    public interface OnListFragmentInteractionListener {

        void onListFragmentInteraction(Article item, ImageView sharedImageView);
    }
}
