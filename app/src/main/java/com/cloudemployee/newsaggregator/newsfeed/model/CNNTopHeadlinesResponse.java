package com.cloudemployee.newsaggregator.newsfeed.model;

import java.util.List;

/**
 * Created by MacBook on 21/03/2019.
 */

public class CNNTopHeadlinesResponse {

    private String status;
    private int totalResults;
    private List<Article> articles;

    public List<Article> getArticles() {
        return articles;
    }

    public int getTotalResults() {
        return totalResults;
    }
}
