package com.cloudemployee.newsaggregator.database.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Query;

import com.cloudemployee.newsaggregator.newsfeed.model.Article;

import java.util.List;

/**
 * Created by MacBook on 21/03/2019.
 */

@Dao
public interface ArticlesDao {

    @Query("SELECT * FROM articles")
    List<Article> getAll();
}
