package com.cloudemployee.newsaggregator.database;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

import com.cloudemployee.newsaggregator.database.dao.ArticlesDao;
import com.cloudemployee.newsaggregator.newsfeed.model.Article;

/**
 * Created by MacBook on 21/03/2019.
 */
@Database(entities = {Article.class}, version = 1)
public abstract class NewsAggregatorRoomDatabase extends RoomDatabase {

    public abstract ArticlesDao articlesDao();

}
