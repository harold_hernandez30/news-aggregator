package com.cloudemployee.newsaggregator;

import android.app.Application;

import com.cloudemployee.newsaggregator.di.component.AppComponent;
import com.cloudemployee.newsaggregator.di.component.DaggerAppComponent;
import com.cloudemployee.newsaggregator.di.module.ApplicationModule;

/**
 * Created by MacBook on 21/03/2019.
 */

public class NewsAggregatorApplication extends Application {

    private AppComponent mAppComponent;

    @Override
    public void onCreate() {
        super.onCreate();

        mAppComponent = DaggerAppComponent.builder()
                .applicationModule(new ApplicationModule(this))
                .build();
    }

    public AppComponent getAppComponent() {
        return mAppComponent;
    }
}
