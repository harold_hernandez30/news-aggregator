package com.cloudemployee.newsaggregator.network;

import com.cloudemployee.newsaggregator.Config;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


/**
 * Created by MacBook on 21/03/2019.
 */

@Module
public class ApiModule {

    @Provides
    @Singleton
    public ApiService apiService(Retrofit retrofit) {
        return retrofit.create(ApiService.class);
    }

    @Provides
    @Singleton
    OkHttpClient provideOkHttpClient(ApiHeaders interceptor) {
        OkHttpClient.Builder builder = new OkHttpClient.Builder();
        builder.addInterceptor(interceptor);

        builder.readTimeout(60, TimeUnit.SECONDS);
        builder.connectTimeout(60, TimeUnit.SECONDS);
        return builder.build();
    }

    @Provides
    @Singleton
    Retrofit provideRetrofit(OkHttpClient client, GsonConverterFactory gsonConverterFactory) {
        return new Retrofit.Builder()
                .client(client)
                .baseUrl(Config.CNN.CNN_BASE_URL)
                .addConverterFactory(gsonConverterFactory)
                .build();
    }


    @Provides
    @Singleton
    public ApiHeaders provideInterceptor() {
        Map<String, String> headers = new HashMap<>();
        headers.put("x-api-key", Config.CNN.CNN_API_KEY);
        ApiHeaders apiHeaders = new ApiHeaders(headers);
        return apiHeaders;
    }

}
