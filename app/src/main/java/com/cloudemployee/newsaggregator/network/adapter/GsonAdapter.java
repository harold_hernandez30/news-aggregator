package com.cloudemployee.newsaggregator.network.adapter;

import com.cloudemployee.newsaggregator.Config;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

import java.lang.reflect.Type;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

/**
 * Created by MacBook on 21/03/2019.
 */

public class GsonAdapter implements JsonSerializer<Date>,JsonDeserializer<Date> {

    public GsonAdapter() {
    }

    @Override
    public synchronized Date deserialize(JsonElement jsonElement, Type type, JsonDeserializationContext jsonDeserializationContext) {
        try {

            SimpleDateFormat dateFormat = new SimpleDateFormat(Config.DATE_FORMAT_24, Locale.US);      //This is the format I need
            dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
            return dateFormat.parse(jsonElement.getAsString());
        } catch (ParseException e) {
            throw new JsonParseException(e);
        }
    }

    @Override
    public JsonElement serialize(Date src, Type typeOfSrc, JsonSerializationContext context) {

        SimpleDateFormat dateFormat = new SimpleDateFormat(Config.DATE_FORMAT_24, Locale.US);      //This is the format I need
        dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        return new JsonPrimitive(dateFormat.format(src));
    }
}
