package com.cloudemployee.newsaggregator.network;

import com.cloudemployee.newsaggregator.newsfeed.model.CNNTopHeadlinesResponse;

import java.util.Map;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;
import retrofit2.http.QueryMap;

/**
 * Created by MacBook on 21/03/2019.
 */

public interface ApiService {
//
//    @GET("top-headlines")
//    Call<CNNTopHeadlinesResponse> requestTopHeadlines(@Query("country") String country);


    @GET("top-headlines")
    Call<CNNTopHeadlinesResponse> requestTopHeadlines(@QueryMap Map<String, String> options);
}
