package com.cloudemployee.newsaggregator.network;

import java.io.IOException;
import java.util.Map;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;


/**
 * Created by MacBook on 21/03/2019.
 */

public class ApiHeaders implements Interceptor {
    private Map<String, String> headers;

    public ApiHeaders(Map<String, String> headers) {
        this.headers = headers;
    }

    @Override
    public Response intercept(Chain chain) throws IOException {

        Request original = chain.request();

        Request.Builder builder = original.newBuilder();

        assignHeaders(builder, headers);
        builder.method(original.method(), original.body());

        return chain.proceed(builder.build());
    }

    private void assignHeaders(Request.Builder builder, Map<String, String> values) {
        if (values == null) return;

        for (Map.Entry<String, String> stringStringEntry : values.entrySet()) {
            builder.header(stringStringEntry.getKey(), stringStringEntry.getValue());
        }
    }
}
