package com.cloudemployee.newsaggregator;

/**
 * Created by MacBook on 21/03/2019.
 */

public class Config {

    public static final int PAGE_SIZE = 3;
    public static String DATE_FORMAT_24 = "yyyy-MM-dd'T'HH:mm:ss'Z'";
    public static String DATE_FORMAT_12 = "yyyy-MM-dd'T'hh:mm:ss'Z'";

    public static class CNN {
        public static String CNN_BASE_URL = "https://newsapi.org/v2/";
        public static String CNN_API_KEY = "c0806c0c3cce44b287ed7de27de1dd7f";
        public static String DB_NAME = "cnn_database";
    }


}
