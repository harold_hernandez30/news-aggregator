package com.cloudemployee.newsaggregator;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;

import com.cloudemployee.newsaggregator.newsdetail.NewsDetailFragment;
import com.cloudemployee.newsaggregator.newsfeed.NewsListingFragment;
import com.cloudemployee.newsaggregator.newsfeed.model.Article;

public class MainActivity extends AppCompatActivity
        implements NewsListingFragment.OnListFragmentInteractionListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        replaceFragment(NewsListingFragment.newInstance(), null, false);
    }

    @Override
    public void onListFragmentInteraction(Article item, ImageView sharedImageView) {
        NewsDetailFragment detailFragment = NewsDetailFragment.newInstance(item, ViewCompat.getTransitionName(sharedImageView));
        replaceFragment(detailFragment, sharedImageView, true);
    }


    private void replaceFragment(Fragment fragment, ImageView sharedImageView, boolean addToBackStack){
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        if (addToBackStack) {
            transaction.addToBackStack(null);
        }
        if (sharedImageView != null) {
            transaction.addSharedElement(sharedImageView, ViewCompat.getTransitionName(sharedImageView));
        }
        transaction.replace(R.id.container, fragment);
        transaction.commit();
    }
}
