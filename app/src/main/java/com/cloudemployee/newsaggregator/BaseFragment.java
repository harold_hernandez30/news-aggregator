package com.cloudemployee.newsaggregator;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;

import com.cloudemployee.newsaggregator.di.component.DaggerFragmentComponent;
import com.cloudemployee.newsaggregator.di.component.FragmentComponent;

/**
 * A simple {@link Fragment} subclass.
 */
public class BaseFragment extends Fragment {


    private FragmentComponent mFragmentComponent;

    public BaseFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mFragmentComponent = DaggerFragmentComponent.builder()
                .appComponent(getApp().getAppComponent())
                .build();
    }

    private NewsAggregatorApplication getApp() {
        return (NewsAggregatorApplication) getActivity().getApplicationContext();
    }

    protected FragmentComponent getFragmentComponent() {
        return mFragmentComponent;
    }
}
