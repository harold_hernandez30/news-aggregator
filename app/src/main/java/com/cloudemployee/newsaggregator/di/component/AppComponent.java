package com.cloudemployee.newsaggregator.di.component;

import android.content.Context;

import com.cloudemployee.newsaggregator.database.NewsAggregatorRoomDatabase;
import com.cloudemployee.newsaggregator.di.module.ApplicationModule;
import com.cloudemployee.newsaggregator.network.ApiModule;
import com.cloudemployee.newsaggregator.network.ApiService;
import com.cloudemployee.newsaggregator.newsfeed.CnnNewsRepository;
import com.cloudemployee.newsaggregator.newsfeed.NewsListingFragment;

import javax.inject.Singleton;

import dagger.Component;
import retrofit2.Retrofit;

@Singleton
@Component(modules = {ApplicationModule.class, ApiModule.class})
public interface AppComponent {
    Retrofit provideRetrofit();

    Context provideAppContext();

    ApiService provideApiService();

    NewsAggregatorRoomDatabase provideNewsAggregatorRoomDatabase();

    CnnNewsRepository provideCnnNewsRepository();

}
