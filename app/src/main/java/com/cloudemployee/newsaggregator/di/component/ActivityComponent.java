package com.cloudemployee.newsaggregator.di.component;


import com.cloudemployee.newsaggregator.MainActivity;
import com.cloudemployee.newsaggregator.di.scope.ActivityScope;

import dagger.Component;

@ActivityScope
@Component(dependencies = AppComponent.class)
public interface ActivityComponent extends AppComponent {

    void inject(MainActivity feedActivity);

}
