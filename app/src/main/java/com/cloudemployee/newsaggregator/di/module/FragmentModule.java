package com.cloudemployee.newsaggregator.di.module;

import android.support.v4.app.Fragment;

import dagger.Module;

@Module
public class FragmentModule {

    final Fragment mFragment;

    public FragmentModule(Fragment fragment) {
        mFragment = fragment;
    }

}
