package com.cloudemployee.newsaggregator.di.module;

import android.arch.persistence.room.Room;
import android.content.Context;

import com.cloudemployee.newsaggregator.Config;
import com.cloudemployee.newsaggregator.NewsAggregatorApplication;
import com.cloudemployee.newsaggregator.database.NewsAggregatorRoomDatabase;
import com.cloudemployee.newsaggregator.network.ApiService;
import com.cloudemployee.newsaggregator.network.adapter.GsonAdapter;
import com.cloudemployee.newsaggregator.newsfeed.CnnNewsRepository;
import com.cloudemployee.newsaggregator.newsfeed.local.CnnNewsLocalDatasource;
import com.cloudemployee.newsaggregator.newsfeed.remote.CnnNewsRemoteDatasource;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.Date;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import retrofit2.converter.gson.GsonConverterFactory;

@Module
public class ApplicationModule {

    private final NewsAggregatorApplication mApp;

    public ApplicationModule(NewsAggregatorApplication app) {
        mApp = app;
    }

    @Provides
    @Singleton
    public Context appContext() {
        return mApp;
    }

    @Provides
    @Singleton
    GsonConverterFactory provideGsonConverter() {

        Gson gson = new GsonBuilder()
                .setDateFormat(Config.DATE_FORMAT_24)
                .registerTypeAdapter(Date.class,
                        new GsonAdapter()).setDateFormat(Config.DATE_FORMAT_24)
                .create();

        return GsonConverterFactory.create(gson);
    }

    @Provides
    @Singleton
    CnnNewsRepository cnnNewsRepository(ApiService apiService) {
        return new CnnNewsRepository(new CnnNewsLocalDatasource(),
                        new CnnNewsRemoteDatasource(apiService));
    }

    @Provides
    @Singleton
    NewsAggregatorRoomDatabase newsAggregatorRoomDatabase(Context context) {
        return Room.databaseBuilder(context,
                NewsAggregatorRoomDatabase.class,
                Config.CNN.DB_NAME).build();
    }
}
