package com.cloudemployee.newsaggregator.di.component;


import com.cloudemployee.newsaggregator.di.scope.FragmentScope;
import com.cloudemployee.newsaggregator.newsfeed.NewsListingFragment;

import dagger.Component;

@FragmentScope
@Component(dependencies = AppComponent.class)
public interface FragmentComponent extends AppComponent {

    void inject(NewsListingFragment newsListingFragment);

}
