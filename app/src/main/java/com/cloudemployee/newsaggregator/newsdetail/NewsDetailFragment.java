package com.cloudemployee.newsaggregator.newsdetail;


import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.transition.TransitionInflater;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.cloudemployee.newsaggregator.R;
import com.cloudemployee.newsaggregator.newsfeed.model.Article;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

public class NewsDetailFragment extends Fragment {
    private static final String ARG_ARTICLE = "article";
    private static final String ARG_TRANSITION_NAME = "transition-name";


    private Article mArticle;
    private String mTransitionName;


    public NewsDetailFragment() {
        // Required empty public constructor
    }

    public static NewsDetailFragment newInstance(Article article, String transitionName) {
        NewsDetailFragment fragment = new NewsDetailFragment();
        Bundle args = new Bundle();
        args.putSerializable(ARG_ARTICLE, article);
        args.putString(ARG_TRANSITION_NAME, transitionName);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        postponeEnterTransition();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            setSharedElementEnterTransition(TransitionInflater.from(getContext()).inflateTransition(android.R.transition.move));
        }

        if (getArguments() != null) {
            mArticle = (Article) getArguments().getSerializable(ARG_ARTICLE);
            mTransitionName = getArguments().getString(ARG_TRANSITION_NAME);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_news_detail, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        TextView author = view.findViewById(R.id.newsdetail_author);
        TextView title = view.findViewById(R.id.newsdetail_article_title);
        TextView content = view.findViewById(R.id.newsdetail_article_content);
        ImageView image = view.findViewById(R.id.newsdetail_image);

        author.setText("By: " + mArticle.getAuthor());
        title.setText(mArticle.getTitle());
        content.setText(mArticle.getContent());

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            image.setTransitionName(mTransitionName);
        }

        Picasso.with(getContext())
                .load(mArticle.getUrlToImage())
                .noFade()
                .centerCrop()
                .fit()
                .into(image, new Callback() {
                    @Override
                    public void onSuccess() {
                        startPostponedEnterTransition();
                    }

                    @Override
                    public void onError() {
                        startPostponedEnterTransition();
                    }
                });
    }
}
